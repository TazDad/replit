import sys
import datetime
import random
from replit import db as diary


def get_salt():
  return random.randint(10000, 11000)


def passwords_match(user, password):
  return all([
    user in diary.keys(),
    hash_password(
      password,
      diary[user]['user_info']['salt']) == diary[user]['user_info']['password']
  ])


def login_user():
  username = input('Username: > ')
  password = input(f'{username}\'s Password: > ')
  if passwords_match(username, password):
    print(f'Welcome back {username.capitalize()}.')
  else:
    print('Invalid username or password, exiting...')
    quit()
  return username


def valid_username(user, user_list):
  return len(user) > 2 and user not in user_list


def get_username(user_list):
  username = 'a'
  while not valid_username(username, user_list):
    username = input('Username: > ').casefold()
  return username


def valid_password(password):
  return 8 < len(password) < 16


def get_password(user):
  password = 'a'
  while not valid_password(password):
    password = input(f'{user}\'s Password: > ')
  return password


def hash_password(password, salt):
  return hash(f'{password}{salt}')


def add_user():
  print('Setting up a new diary.')
  username = get_username(diary.keys())
  password = get_password(username)
  salt = get_salt()
  hashed_password = hash_password(password, salt)
  diary[username] = {
    'user_info': {
      'password': hashed_password,
      'salt': salt
    },
    'diary': {}
  }
  return username


def add_entry(user):
  diary[user]['diary'][str(datetime.datetime.now())] = input(
    'Enter your entry to your diary: ')
  return True

def view_entry(user):
  count = 0
  for k in sorted(diary[user]['diary'].keys(), reverse=True):
    print(k, diary[user]['diary'][k])
    count += 1
    if count % 5 == 0 and count < len(diary[user]['diary'].keys()):
      response = 'a'
      while response not in 'ny':
        response = input(
          f'Would you like to see the next group of entries (Y/N): ')[0].casefold()
      if response == 'n':
        break
  return True

def diary_logout(user):
  return False

def diary_exit():
  sys.exit()

def verify_access():
#  del(diary['marco'])
#  del(diary['bill'])
#  print(diary.keys())
  if len(diary.keys()) == 0:
    return add_user()
  access_options = {'Add User': add_user, 'Login': login_user, 'Quit': diary_exit}
  action = 'c'
  while action not in [c[0].casefold() for c in access_options.keys()]:
    action = input(
      f'Entry to diary type ({", ".join(access_options.keys())}): ')[0].casefold()
  action = [access_options[k] for k in access_options.keys()
            if k[0].casefold() == action][0]
  return action()


def run_diary():
  user = verify_access()
  diary_entries = True
  options = {'Add': add_entry, 'View': view_entry, 'Quit': diary_logout}
  while diary_entries:
    action = 'c'
    while action not in [c[0].casefold() for c in options.keys()]:
      action = input(
        f'What action do you want to perform ({", ".join(options.keys())}): ')[0].casefold()
    action = [options[k] for k in options.keys()
              if k[0].casefold() == action][0]
    diary_entries = action(user)


while True:
  run_diary()
